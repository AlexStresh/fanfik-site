﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FanfikSite.Models;
using FanfikSite.Models.HomeViewModels;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;

namespace FanfikSite.Controllers
{
    public class HomeController : BaseController
    {
        private const int PageCount = 12; 

        public HomeController(IServiceProvider provider) : base(provider)
        {           
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }

        public IActionResult Index()
        {
            var fanfiks = ApplicationDbContext.Fanfik.OrderByDescending(f => f.Updated).ToList();
            if (fanfiks.Count > PageCount)
                fanfiks = fanfiks.GetRange(0, PageCount);

            var model = new IndexViewModel()
            {
                Fanfiks = fanfiks,
                Tags = ApplicationDbContext.Tag.ToList()
            };
            return View(model);
        }

        public JsonResult SearchAjax(string search)
        {
            if (search != null)
            {
                List<Fanfik> fanfiks;
                if (search.StartsWith("#"))
                {
                    fanfiks = ApplicationDbContext.FanfikTag
                        .Where(ft => ft.Tag.Name == search.Replace("#", ""))
                        .Select(ft => ft.Fanfik)
                        .Include(f => f.Chapters)
                        .ToList();
                }
                else
                {
                    fanfiks = ApplicationDbContext.Fanfik
                        .Where(f => f.Title.ToLower().StartsWith(search))
                        .Include(f => f.Chapters)
                        .ToList();
                }
                
                return Json(fanfiks);
            }
            return Json("");
        }

        public IActionResult Search(string search)
        {
            if(search != null)
            {
                List<Fanfik> fanfiks;
                if (search.StartsWith("#"))
                {
                    fanfiks = ApplicationDbContext.FanfikTag
                        .Where(ft => ft.Tag.Name == search.Replace("#", ""))
                        .Select(ft => ft.Fanfik)
                        .Include(f => f.Chapters)
                        .ToList();
                }
                else
                {
                    fanfiks = ApplicationDbContext.Fanfik
                        .Where(f => f.Title.ToLower().StartsWith(search))
                        .Include(f => f.Chapters)
                        .ToList();
                }
                var model = new SearchViewModel()
                {
                    Fanfiks = fanfiks,
                    SearchString = search
                };
                return View(model);
            }
            return NotFound();
        }

        public JsonResult GetPosts(int page, bool isNew)
        {
            var index = page * PageCount;
            var fanfiks = ApplicationDbContext.Fanfik.ToList();

            if (isNew)
                fanfiks = fanfiks.OrderByDescending(f => f.Updated).ToList();
            else
                fanfiks = fanfiks.OrderByDescending(f => f.Rating).ToList();

            if(index + PageCount > fanfiks.Count)
            {
                if (fanfiks.Count - index <= 0)
                    return Json(-1);
                else
                    return Json(fanfiks.GetRange(index, fanfiks.Count - index));
            }
            return Json(fanfiks.GetRange(index, PageCount));
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
