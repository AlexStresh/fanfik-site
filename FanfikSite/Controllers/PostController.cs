﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using FanfikSite.Models.PostViewModels;
using FanfikSite.Models;
using Dropbox.Api.Files;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using WkWrap.Core;
using FanfikSite.Services;

namespace FanfikSite.Controllers
{
    public class PostController : BaseController
    {
        private readonly IViewRenderService _viewRenderService;

        public PostController(IServiceProvider provider, IViewRenderService viewRenderService) : base(provider)
        {
            _viewRenderService = viewRenderService;
        }

        [HttpGet]
        [Authorize]
        public IActionResult CreatePost(string id)
        {
            var model = new PostViewModel()
            {
                Genres = ApplicationDbContext.Genre.ToList(),
                CreatorId = id ?? CurrentUser.Id
            };
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreatePost(PostViewModel model)
        {
            if (ModelState.IsValid)
            {
                Fanfik fanfik = new Fanfik
                {
                    Id = Guid.NewGuid(),
                    Title = model.Title,
                    Shortcut = model.Shortcut,
                    Created = DateTime.Now,
                    Updated = DateTime.Now,
                    Rating = 0,
                    Creator = await ApplicationDbContext.Users.FirstOrDefaultAsync(c => c.Id == model.CreatorId),
                    Genre = await ApplicationDbContext.Genre.FirstOrDefaultAsync(g => g.Name == model.Genre)
                };

                if (model.Photo != null)
                {
                    var link = await SaveImage(model.Photo, fanfik.Id.ToString());
                    fanfik.Image = link;
                }

                AddTags(fanfik, model.Tags);
                ApplicationDbContext.Fanfik.Add(fanfik);
                await ApplicationDbContext.SaveChangesAsync();
                return Redirect("/Post/Get/" + fanfik.Id);
            }

            return View(model);
        }

        [HttpGet]
        [ActionName("Get")]
        public IActionResult GetPost(Guid id)
        {
            if (id != null)
            {
                var fanfik = ApplicationDbContext.Fanfik
                    .Include(f => f.Creator)
                    .FirstOrDefault(f => f.Id == id);
                if (fanfik != null)
                {
                    var model = new GetPostViewModel()
                    {
                        Fanfik = fanfik,
                        Comments = ApplicationDbContext.Comment.Where(c => c.Fanfik == fanfik).ToList(),
                        Tags = ApplicationDbContext.FanfikTag.Where(ft => ft.Fanfik == fanfik).Select(ft => ft.Tag).ToList(),
                        Chapters = ApplicationDbContext.Chapter.Where(c => c.Fanfik == fanfik).ToList(),
                        CurrentUser = CurrentUser
                    };

                    return View(model);
                }
            }
            return NotFound();
        }

        [HttpGet]
        [Authorize]
        public IActionResult AddChapter(Guid fanfikId)
        {
            if (fanfikId == null)
                return NotFound();

            var model = new AddChapterViewModel()
            {
                FanfikFk = ApplicationDbContext.Fanfik.FirstOrDefault(f => f.Id == fanfikId).Id
            };

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddChapter(AddChapterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var chaptersCount = ApplicationDbContext.Chapter.Where(c => c.FanfikFk == model.FanfikFk).ToList().Count;
                var fanfik = ApplicationDbContext.Fanfik.FirstOrDefault(f => f.Id == model.FanfikFk);
                
                var chapter = new Chapter()
                {
                    Id = Guid.NewGuid(),
                    Fanfik = fanfik,
                    Name = model.Name,
                    Context = model.Context,
                    SerialNumber = chaptersCount + 1
                };

                if (model.OptionIlistrutation != null)
                {
                    var link = await SaveImage(model.OptionIlistrutation, Guid.NewGuid().ToString());
                    chapter.OptionIlistrutation = link;
                }

                fanfik.Updated = DateTime.Now;

                ApplicationDbContext.Fanfik.Update(fanfik);
                ApplicationDbContext.Chapter.Add(chapter);
                ApplicationDbContext.SaveChanges();
                return Redirect("/Post/Get/" + chapter.FanfikFk);
            }
            return View(model);
        }

        [HttpGet]
        [ActionName("Chapter")]
        public async Task<IActionResult> GetChapter(Guid id)
        {
            if(id != null)
            {
                var chapter = await ApplicationDbContext.Chapter
                    .Include(c => c.Ratings)
                    .FirstOrDefaultAsync(c => c.Id == id);

                var fanfik = await ApplicationDbContext.Fanfik
                    .Include(f => f.Chapters)
                    .FirstOrDefaultAsync(f => f.Id == chapter.FanfikFk);
                fanfik.Chapters = fanfik.Chapters.OrderBy(c => c.SerialNumber).ToList();

                var commets = ApplicationDbContext.Comment
                    .Include(c => c.Likes)
                    .Include(c => c.Creator)
                    .Where(c => c.Fanfik == fanfik)
                    .OrderBy(c => c.Created);

                var rating = User.Identity
                    .IsAuthenticated ?
                    ApplicationDbContext.Rating.FirstOrDefault(r => r.User == CurrentUser && r.Chapter == chapter) : null;

                List<Guid> likeComments = new List<Guid>();

                if(CurrentUser != null)
                {
                    likeComments = ApplicationDbContext.Like
                    .Include(l => l.Comment)
                    .Include(l => l.Creator)
                    .Where(l => l.Creator == CurrentUser)
                    .Select(l => l.Comment)
                    .Select(c => c.Id)
                    .ToList();
                }

                var model = new GetChapterViewModel()
                {
                    Chapter = chapter,
                    Fanfik = fanfik,
                    Comments = commets.ToList(),
                    Rating = rating,
                    LikeComments = likeComments
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [Authorize]
        public JsonResult AddLike(Guid commentId)
        {
            if (commentId != null)
            {
                var comment = ApplicationDbContext.Comment.FirstOrDefault(c => c.Id == commentId);
                if (commentId != null)
                {
                    var like = new Like()
                    {
                        Id = Guid.NewGuid(),
                        Creator = CurrentUser,
                        Comment = comment
                    };

                    ApplicationDbContext.Like.Add(like);
                    ApplicationDbContext.SaveChanges();
          
                    return Json(ApplicationDbContext.Like.Where(l => l.Comment == comment).Count());
                }
            }
            return Json(-1);
        }

        [HttpPost]
        [Authorize]
        public JsonResult DeleteChapter(Guid id)
        {
            if (id == null)
                return Json(-1);

            var chapter = ApplicationDbContext.Chapter.FirstOrDefault(c => c.Id == id);
            ApplicationDbContext.Chapter.Remove(chapter);
            ApplicationDbContext.SaveChanges();
            return Json(1);
        }

        private void DeleteFanfik(Guid id)
        {
            var fanfik = ApplicationDbContext.Fanfik
                .Include(f => f.Comments)
                .ThenInclude(c => c.Likes)
                .FirstOrDefault(c => c.Id == id);

            var fanfikTag = ApplicationDbContext.FanfikTag
                .Where(ft => ft.Fanfik == fanfik);

            var chapters = ApplicationDbContext.Chapter
                .Include(c => c.Ratings)
                .Where(c => c.Fanfik == fanfik);

            foreach (var chapter in chapters)
                ApplicationDbContext.Rating.RemoveRange(chapter.Ratings);

            foreach (var comment in fanfik.Comments)
                ApplicationDbContext.Like.RemoveRange(comment.Likes);

            ApplicationDbContext.Comment.RemoveRange(fanfik.Comments);
            ApplicationDbContext.Chapter.RemoveRange(chapters);
            ApplicationDbContext.FanfikTag.RemoveRange(fanfikTag);
            ApplicationDbContext.Fanfik.Remove(fanfik);
            ApplicationDbContext.SaveChanges();
        }

        [Authorize]
        public JsonResult GetComments(int count, Guid fanfikId)
        {
            var comments = ApplicationDbContext.Comment
                .Include(c => c.Creator)
                .Where(c => c.FanfikFk == fanfikId)
                .OrderBy(c => c.Created)
                .ToList();
                 
            if(comments.Count > count)
                return Json(comments.GetRange(count, comments.Count - count).ToList());

            return Json(-1);
        }

        [HttpPost]
        [Authorize]
        public JsonResult DeletePostJson(Guid id)
        {
            if (id == null)
                return Json(-1);

            DeleteFanfik(id);

            return Json(0);
        }
        
        [Authorize]
        public IActionResult DeletePost(Guid id)
        {
            var result = DeletePostJson(id);

            return RedirectToAction("Index", "Home");
        }

        private void RecalculateRating(Guid fanfikId)
        {
            var fanfik = ApplicationDbContext.Fanfik
                .Include(f => f.Chapters)
                .ThenInclude(ch => ch.Ratings)
                .FirstOrDefault(f => f.Id == fanfikId);

            float sum = 0;

            foreach(var chapter in fanfik.Chapters)
            {
                float chapterRating = chapter.Ratings.Sum(r => r.Value) / chapter.Ratings.Count;
                sum += chapterRating;
            }

            fanfik.Rating = sum / fanfik.Chapters.Count;

            ApplicationDbContext.Fanfik.Update(fanfik);
            ApplicationDbContext.SaveChanges();
        }

        [HttpPost]
        [Authorize]
        public JsonResult AddRating(Guid chapterId, int value)
        {
            if ((chapterId == null) || (value <= 0))
                return Json(0);

            var exist = ApplicationDbContext.Rating.FirstOrDefault(r => r.ChapterFk == chapterId && r.User == CurrentUser);
            var chapter = ApplicationDbContext.Chapter.FirstOrDefault(c => c.Id == chapterId);

            if (exist == null)
            {
                var rating = new Rating()
                {
                    Id = Guid.NewGuid(),
                    Value = value,
                    Chapter = chapter,
                    User = CurrentUser
                };
                ApplicationDbContext.Rating.Add(rating);
                ApplicationDbContext.SaveChanges();
            }
            else
            {
                exist.Value = value;
                ApplicationDbContext.Rating.Update(exist);
                ApplicationDbContext.SaveChanges();
            }

            RecalculateRating(chapter.FanfikFk);

            return Json(1);
        }

        [HttpPost]
        [Authorize]
        public JsonResult AddComment(Guid fanfikId, string context)
        {
            if ((fanfikId == null) || (context == null))
                return Json(0);

            var comment = new Comment()
            {
                Id = Guid.NewGuid(),
                Content = context,
                Created = DateTime.Now,
                Creator = CurrentUser,
                Fanfik = ApplicationDbContext.Fanfik.FirstOrDefault(f => f.Id == fanfikId)
            };
            ApplicationDbContext.Comment.Add(comment);
            ApplicationDbContext.SaveChanges();
            return Json(comment);
        }

        [HttpGet]
        [Authorize]
        public IActionResult EditPost(Guid id)
        {
            if (id == null)
                return NotFound();

            var post = ApplicationDbContext.Fanfik
                .Include(f => f.Creator)
                .Include(f => f.Genre)
                .FirstOrDefault(f => f.Id == id);

            if (post == null || (!post.Creator.Equals(CurrentUser) && !IsAdmin))
                return NotFound();
            
            var model = new EditPostViewModel
            {
                Id = post.Id,
                Title = post.Title,
                Shortcut = post.Shortcut,
                Image = post.Image,
                Genre = post.Genre.Name,
                Genres = ApplicationDbContext.Genre.ToList()
            };

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> EditPost(EditPostViewModel model)
        {
            if (ModelState.IsValid)
            {
                var post = ApplicationDbContext.Fanfik.FirstOrDefault(p => p.Id == model.Id);
                post.Title = model.Title;
                post.Shortcut = model.Shortcut;
                
                if (model.NewImage != null)
                {
                    var link = await SaveImage(model.NewImage, Guid.NewGuid().ToString());
                    post.Image = link;
                }

                post.Genre = ApplicationDbContext.Genre.FirstOrDefault(g => g.Name == model.Genre);

                ApplicationDbContext.Fanfik.Update(post);
                ApplicationDbContext.SaveChanges();
                return Redirect("/Post/Get/" + post.Id);
            }
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public IActionResult EditChapter(Guid id)
        {
            if (id == null)
                return NotFound();

            var chapter = ApplicationDbContext.Chapter.FirstOrDefault(c => c.Id == id);
            if (chapter == null)
                return NotFound();

            var model = new EditChapterViewModel()
            {
                Id = chapter.Id,
                Name = chapter.Name,
                OptionIlistrutation = chapter.OptionIlistrutation,
                Context = chapter.Context,
                FanfikFk = chapter.FanfikFk
            };

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> EditChapter(EditChapterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var chapter = ApplicationDbContext.Chapter.FirstOrDefault(c => c.Id == model.Id);
                if (chapter != null)
                {
                    chapter.Name = model.Name;
                    chapter.Context = model.Context;

                    if(model.NewOptionIlistrutation != null)
                    {
                        var link = await SaveImage(model.NewOptionIlistrutation, Guid.NewGuid().ToString());
                        chapter.OptionIlistrutation = link;
                    }

                    ApplicationDbContext.Chapter.Update(chapter);
                    ApplicationDbContext.SaveChanges();

                    return Redirect("/Post/Get/" + chapter.FanfikFk);
                }
            }
            return View(model);
        }

        private void AddTags(Fanfik fanfik, string tagsString)
        {
            foreach (var tag in tagsString.Split("#", StringSplitOptions.RemoveEmptyEntries))
            {
                var tagEntity = ApplicationDbContext.Tag
                    .FirstOrDefault(t => t.Name.Equals(tag, StringComparison.InvariantCultureIgnoreCase));
                if (tagEntity == null)
                {
                    tagEntity = new Tag() { Id = Guid.NewGuid(), Name = tag };
                    ApplicationDbContext.Tag.Add(tagEntity);
                }

                var fanfikTag = new FanfikTag() { Id = Guid.NewGuid(), Fanfik = fanfik, Tag = tagEntity };
                ApplicationDbContext.FanfikTag.Add(fanfikTag);
            }
        }

        public IActionResult Pdf(PdfViewModel model)
        {            
            return View(model);
        }

        public async Task<IActionResult> DownloadPdf(Guid id)
        {
            var model = new PdfViewModel()
            {
                Fanfik = ApplicationDbContext.Fanfik
                .Include(f => f.Chapters)
                .Include(f => f.Creator)
                .FirstOrDefault(f => f.Id == id)
            };

            model.Fanfik.Chapters = model.Fanfik.Chapters.OrderBy(c => c.SerialNumber).ToList() ;

            var htmlContent = await _viewRenderService.RenderToStringAsync("Post/Pdf", model);
            var wkhtmltopdf = new FileInfo(@"C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe");
            var converter = new HtmlToPdfConverter(wkhtmltopdf);
            var pdfBytes = converter.ConvertToPdf(htmlContent);

            FileResult fileResult = new FileContentResult(pdfBytes, "application/pdf")
            {
                FileDownloadName = model.Fanfik.Title + ".pdf"
            };
            return fileResult;
        }

    }
}