﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using FanfikSite.Models;
using FanfikSite.Operations;
using FanfikSite.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using FanfikSite.Data;
using Microsoft.Extensions.Configuration;
using Dropbox.Api;
using Microsoft.AspNetCore.Http;
using System.IO;
using Dropbox.Api.Files;

namespace FanfikSite.Controllers
{
    public class BaseController : Controller
    {
        protected IUserOperation UserOperation { get; set; }
        protected UserManager<ApplicationUser> _userManager;
        protected SignInManager<ApplicationUser> _signInManager;
        protected ApplicationDbContext ApplicationDbContext;
        public IConfiguration Configuration { get; }

        protected BaseController(IServiceProvider serviceProvider)
        {
            Configuration = serviceProvider.GetService<IConfiguration>();
            _userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();
            _signInManager = serviceProvider.GetService<SignInManager<ApplicationUser>>();
            ApplicationDbContext = serviceProvider.GetService<ApplicationDbContext>();
            UserOperation = serviceProvider.GetService<IUserOperation>();
        }

        public BaseController() { }

        protected ApplicationUser CurrentUser => UserOperation.GetCurrentUser(User);

        protected bool IsAdmin => UserOperation.IsAdmin(CurrentUser.Id).GetAwaiter().GetResult();
        
        protected DropboxClient DropboxClient => new DropboxClient(Configuration["DropBoxAccessToken"]);

        public async Task<string> SaveImage(IFormFile image, string filename)
        {
            byte[] imageData = null;
            using (var binaryReader = new BinaryReader(image.OpenReadStream()))
            {
                imageData = binaryReader.ReadBytes((int)image.Length);
            }
            var path = Configuration["RootDir"] + "/" + filename;
            using (var stream = new MemoryStream(imageData))
            {
                var response = await DropboxClient.Files.UploadAsync(path, WriteMode.Overwrite.Instance, body: stream);
            }

            var link = await DropboxClient.Sharing.CreateSharedLinkWithSettingsAsync(path);

            return link.Url.Replace("dl=0", "raw=1");
        }
    }
}