﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using FanfikSite.Models.UserViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using FanfikSite.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FanfikSite.Controllers
{
    public class UserController : BaseController
    {
        public UserController(IServiceProvider provider) : base(provider)
        {
        }

        [HttpPost]
        public async Task<JsonResult> DeleteUser(string id)
        {
            if (id != null || !IsAdmin)
            {
                var user = ApplicationDbContext.Users.FirstOrDefault(u => u.Id == id);
                if (user != null)
                {
                    user.IsDeleted = true;
                    ApplicationDbContext.Users.Update(user);
                    ApplicationDbContext.SaveChanges();
                    await _userManager.UpdateSecurityStampAsync(user);
                    await _userManager.SetLockoutEndDateAsync(user, DateTimeOffset.MaxValue);

                    return Json(1);
                }
            }
            return Json(0);
        }

        [HttpPost]
        public async Task<JsonResult> BlockUser(string id)
        {
            if (id != null || !IsAdmin)
            {
                var user = ApplicationDbContext.Users.FirstOrDefault(u => u.Id == id);
                await _userManager.SetLockoutEndDateAsync(user, (DateTimeOffset)DateTime.Now.AddDays(7));
                await _userManager.UpdateSecurityStampAsync(user);
                return Json(1);
            }
            return Json(0);
        }

        [HttpPost]
        public JsonResult UnblockUser(string id)
        {
            if (id != null || !IsAdmin)
            {
                var user = ApplicationDbContext.Users.FirstOrDefault(u => u.Id == id);
                _userManager.SetLockoutEndDateAsync(user, DateTimeOffset.UtcNow).GetAwaiter().GetResult();
                return Json(1);
            }
            return Json(0);
        }

        [HttpPost]
        public async Task<JsonResult> AddAdmin(string id)
        {
            if (id != null || !IsAdmin)
            {
                var user = ApplicationDbContext.Users.FirstOrDefault(u => u.Id == id);
                await _userManager.AddToRoleAsync(user, "Admin");
                return Json(1);
            }
            return Json(0);
        }

        [HttpGet]
        public IActionResult Index()
        {
            if (!IsAdmin)
            {
                var users = ApplicationDbContext.Users
                .Where(u => u.IsDeleted != true)
                .Include(u => u.Fanfiks)
                .ToList();

                var model = new UsersViewModel();

                foreach (var user in users)
                {
                    var newUser = new Users()
                    {
                        User = user,
                        Roles = _userManager.GetRolesAsync(user).GetAwaiter().GetResult() as List<string>
                    };
                    model.Users.Add(newUser);
                }
                return View(model);
            }
            return NotFound();
        }

        [HttpGet]
        [ActionName("Profile")]
        public async Task<IActionResult> GetUserProfile(string id)
        {
            ApplicationUser user;
            if(id == null)
            {
                user = ApplicationDbContext.Users
                    .Include(u => u.Fanfiks)
                    .Include(u => u.Comments)
                    .FirstOrDefault(u => u.Id == CurrentUser.Id);
            }
            else
            {
                user = ApplicationDbContext.Users
                    .Include(u => u.Fanfiks)
                    .Include(u => u.Comments)
                    .FirstOrDefault(u => u.Id == id);
            }

            var model = new UserViewModel()
            {
                User = user,
                IsAdmin = await _userManager.IsInRoleAsync(user, "Admin")
            };

            return View(model);
        }

        [Authorize]
        public JsonResult ChangeTheme(string id)
        {
            var user = ApplicationDbContext.Users.FirstOrDefault(u => u.Id == id);
            if(user != null)
            {
                if (user.Theme == "Dark")
                    user.Theme = "Light";
                else
                    user.Theme = "Dark";

                ApplicationDbContext.Users.Update(user);
                ApplicationDbContext.SaveChanges();
                return Json(1);
            }
            return Json(0);
        }

        [Authorize]
        public async Task<IActionResult> Admin()
        {
            if (!IsAdmin)
                return NotFound();

            var users = ApplicationDbContext.Users
                .Include(u => u.Fanfiks)
                .Include(u => u.Comments)
                .ToList();

            var model = new UsersViewModel() { Users = new List<Users>() };

            foreach (var user in users)
            {
                var newUser = new Users()
                {
                    User = user,
                    Roles = await _userManager.GetRolesAsync(user) as List<string>
                };
                model.Users.Add(newUser);
            }
            return View(model);
        }

        public IActionResult Statistics()
        {
            if (!IsAdmin)
                return NotFound();

            float todayFanfikCount = ApplicationDbContext.Fanfik
                .Where(f => f.Updated >= DateTime.Now.AddHours(-24))
                .Count();
                    
            float todayUserCount = ApplicationDbContext.Users
                .Where(u => u.Created >= DateTime.Now.AddHours(-24))
                .Count();

            float todayCommentCount = ApplicationDbContext.Comment
                .Where(c => c.Created >= DateTime.Now.AddHours(-24))
                .Count();

            float mounthFanfikCount = ApplicationDbContext.Fanfik
                .Where(f => f.Updated >= DateTime.Now.AddDays(-30))
                .Count();

            float mounthUserCount = ApplicationDbContext.Users
                .Where(u => u.Created >= DateTime.Now.AddDays(-30))
                .Count();

            float mounthCommentCount = ApplicationDbContext.Comment
                .Where(c => c.Created >= DateTime.Now.AddDays(-30))
                .Count();

            float totalFanfikCount = ApplicationDbContext.Fanfik.Count();
            float totalUserCount = ApplicationDbContext.Users.Count();
            float totalCommentCount = ApplicationDbContext.Comment.Count();

            var model = new StatViewModel()
            {
                TodayFanfiksCount = todayFanfikCount,
                TodayUserCount = todayUserCount,
                TodayCommentCount = todayCommentCount,

                MounthFanfiksCount = mounthFanfikCount,
                MounthCommentCount = mounthCommentCount,
                MounthUserCount = mounthUserCount,

                FanfiksCount = totalFanfikCount,
                UsersCount = totalUserCount,
                CommentsCount = totalCommentCount
            };
            return View(model);
        }
    }
}