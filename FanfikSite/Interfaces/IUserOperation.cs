﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using FanfikSite.Models;

namespace FanfikSite.Interfaces
{
    public interface IUserOperation
    {
        ApplicationUser GetCurrentUser(ClaimsPrincipal identityUser);
        Task<bool> IsAdmin(string userId);
    }
}
