﻿using FanfikSite.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanfikSite.Models;
using System.Security.Claims;
using FanfikSite.Data;
using Microsoft.AspNetCore.Identity;

namespace FanfikSite.Operations
{
    public class UserOperation : IUserOperation
    {
        private readonly ApplicationDbContext _ctx;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserOperation(ApplicationDbContext ctx, UserManager<ApplicationUser> userManager)
        {
            _ctx = ctx;
            _userManager = userManager;
        }

        public ApplicationUser GetCurrentUser(ClaimsPrincipal identityUser)
        {
            if (identityUser == null) return null;

            ApplicationUser currentUser = _ctx.Users.FirstOrDefault(u => u.Email == identityUser.Identity.Name);
            return currentUser;
        }

        public async Task<bool> IsAdmin(string userId)
        {
            var user = _ctx.Users.FirstOrDefault(u => u.Id == userId);
            var userRoles = await _userManager.GetRolesAsync(user);
            if (userRoles.Contains("Admin"))
                return true;

            return false;
        }
    }
}
