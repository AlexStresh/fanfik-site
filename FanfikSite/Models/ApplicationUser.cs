﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;

namespace FanfikSite.Models
{
    
    public class ApplicationUser : IdentityUser
    {

        public ApplicationUser()
        {
            Comments = new HashSet<Comment>();
            Fanfiks = new HashSet<Fanfik>();
            Ratings = new HashSet<Rating>();
            Likes = new HashSet<Like>();
        }

        public string Avatar { get; set; }

        public bool IsDeleted { get; set; }

        public string Theme { get; set; }

        public DateTime Created { get; set; }

        public ICollection<Comment> Comments { get; set; }
        public ICollection<Fanfik> Fanfiks { get; set; }
        public ICollection<Rating> Ratings { get; set; }
        public ICollection<Like> Likes { get; set; }
    }
}
