﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models
{
    public class Like
    {
        public Guid Id { get; set; }

        public Guid CommnetFk { get; set; }
        public Comment Comment { get; set; }

        public string CreatorFk { get; set; }
        public ApplicationUser Creator { get; set; }
    }
}
