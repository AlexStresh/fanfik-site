﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models
{
    public partial class Rating
    {
        public Guid Id { get; set; }

        public int Value { get; set; }
        public string UserFk { get; set; }
        public Guid ChapterFk { get; set; }
        
        public Chapter Chapter { get; set; }
        public ApplicationUser User { get; set; }
    }
}
