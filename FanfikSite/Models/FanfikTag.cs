﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models
{
    public partial class FanfikTag
    {
        public Guid Id { get; set; }
        public Guid FanfikFk { get; set; }
        public Guid TagFk { get; set; }

        public Fanfik Fanfik { get; set; }
        public Tag Tag { get; set; }
    }
}
