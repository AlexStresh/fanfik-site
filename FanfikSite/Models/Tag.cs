﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models
{
    public partial class Tag
    {
        public Tag()
        {
            FanfikTags = new HashSet<FanfikTag>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public ICollection<FanfikTag> FanfikTags { get; set; }
    }
}
