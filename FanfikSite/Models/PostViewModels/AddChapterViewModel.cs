﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models.PostViewModels
{
    public class AddChapterViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public IFormFile OptionIlistrutation { get; set; }
        public string Context { get; set; }
        public Guid FanfikFk { get; set; }
    }
}
