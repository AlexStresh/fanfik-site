﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models.PostViewModels
{
    public class EditPostViewModel
    {
        public Guid Id { get; set; }
        
        [Required]
        [StringLength(120, MinimumLength = 3)]
        public string Title { get; set; }

        [Required]
        public string Shortcut { get; set; }

        public string Image { get; set; }

        public IFormFile NewImage { get; set; }

        [Required]
        public string Genre { get; set; }

        public List<Genre> Genres { get; set; }

    }
}
