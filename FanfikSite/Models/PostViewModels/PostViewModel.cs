﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace FanfikSite.Models.PostViewModels
{
    public class PostViewModel
    {
        [Required]
        [StringLength(120, MinimumLength = 3)]
        public string Title { get; set; }

        [Required]
        public string Shortcut { get; set; }

        public IFormFile Photo { get; set; }

        [Required]
        public string Genre { get; set; }
        
        [Required]
        public string Tags { get; set; }

        public string CreatorId { get; set; }
        public List<Genre> Genres { get; set; }
    }
}
