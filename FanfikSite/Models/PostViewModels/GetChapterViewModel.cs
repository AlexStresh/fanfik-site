﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanfikSite.Models;

namespace FanfikSite.Models.PostViewModels
{
    public class GetChapterViewModel
    {
        public Chapter Chapter { get; set; }
        public Fanfik Fanfik { get; set; }
        public List<Comment> Comments { get; set; }
        
        public List<Guid> LikeComments { get; set; } 

        public Rating Rating { get; set; }
    }
}
