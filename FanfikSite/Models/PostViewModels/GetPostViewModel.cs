﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanfikSite.Models;
using FanfikSite.Data;

namespace FanfikSite.Models.PostViewModels
{
    public class GetPostViewModel
    {
        public Fanfik Fanfik { get; set; }
        public List<Tag> Tags { get; set; }
        public List<Comment> Comments { get; set; }
        public List<Chapter> Chapters { get; set; }
        public ApplicationUser CurrentUser { get; set; }

    }
}
