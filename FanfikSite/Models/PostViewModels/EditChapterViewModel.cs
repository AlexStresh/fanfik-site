﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models.PostViewModels
{
    public class EditChapterViewModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string OptionIlistrutation { get; set; }

        public IFormFile NewOptionIlistrutation { get; set; }

        [Required]
        public string Context { get; set; }

        public Guid FanfikFk { get; set; }
    }
}
