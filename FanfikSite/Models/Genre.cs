﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models
{
    public class Genre
    {

        public Genre()
        {
            Fanfiks = new HashSet<Fanfik>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public ICollection<Fanfik> Fanfiks { get; set; }
    }
}
