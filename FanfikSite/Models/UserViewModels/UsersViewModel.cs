﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanfikSite.Models;

namespace FanfikSite.Models.UserViewModels
{
    public class UsersViewModel
    {
        public List<Users> Users { get; set; }
        public SortViewModel SortViewModel { get; set; }
    }

    public class Users
    {
        public ApplicationUser User { get; set; }
        public List<string> Roles { get; set; }
    }
}