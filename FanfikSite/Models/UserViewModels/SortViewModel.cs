﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models.UserViewModels
{
    public class SortViewModel
    {
        public SortState FanfikCountSort { get; set; }
        public SortState UsernameSort { get; set; }
        public SortState EmailSort { get; set; }
        public SortState Current { get; set; }
        public bool Up { get; set; }

        public SortViewModel(SortState sortOrder)
        {
            FanfikCountSort = SortState.FanfikCountAsc;
            UsernameSort = SortState.UsernameAsc;
            EmailSort = SortState.EmailAsc;
            Up = true;

            if (sortOrder == SortState.FanfikCountDesc || sortOrder == SortState.UsernameDesc
                || sortOrder == SortState.EmailDesc)
            {
                Up = false;
            }

            switch (sortOrder)
            {
                case SortState.FanfikCountDesc:
                    Current = FanfikCountSort = SortState.FanfikCountAsc;
                    break;
                case SortState.UsernameAsc:
                    Current = UsernameSort = SortState.UsernameDesc;
                    break;
                case SortState.UsernameDesc:
                    Current = UsernameSort = SortState.UsernameAsc;
                    break;
                case SortState.EmailAsc:
                    Current = EmailSort = SortState.EmailDesc;
                    break;
                case SortState.EmailDesc:
                    Current = EmailSort = SortState.EmailAsc;
                    break;
                default:
                    Current = FanfikCountSort = SortState.FanfikCountDesc;
                    break;
            }
        }
    }
}