﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models.UserViewModels
{
    public class StatViewModel
    {
        public float TodayFanfiksCount { get; set; }
        public float TodayUserCount { get; set; }
        public float TodayCommentCount { get; set; }

        public float MounthFanfiksCount { get; set; }
        public float MounthUserCount { get; set; }
        public float MounthCommentCount { get; set; }

        public float FanfiksCount { get; set; }
        public float UsersCount { get; set; }
        public float CommentsCount { get; set; }
    }
}
