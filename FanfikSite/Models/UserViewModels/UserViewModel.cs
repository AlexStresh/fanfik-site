﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanfikSite.Models;

namespace FanfikSite.Models.UserViewModels
{
    public class UserViewModel
    {
        public ApplicationUser User { get; set; }
        public bool IsAdmin { get; set; }

    }
}
