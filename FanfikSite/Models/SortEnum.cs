﻿public enum SortState
{
    UsernameAsc,
    UsernameDesc,
    FanfikCountAsc,
    FanfikCountDesc,
    EmailAsc,
    EmailDesc
}