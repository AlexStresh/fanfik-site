﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models.HomeViewModels
{
    public class SearchViewModel
    {
        public List<Fanfik> Fanfiks { get; set; }
        public string SearchString { get; set; }

    }
}
