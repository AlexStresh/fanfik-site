﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models.HomeViewModels
{
    public class IndexViewModel
    {
        public List<Fanfik> Fanfiks { get; set; }
        public List<Tag> Tags { get; set; }
    }
}
