﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models
{
    public partial class Chapter
    {

        public Chapter()
        {
            Ratings = new HashSet<Rating>();
        }

        public Guid Id{ get; set; }
        public string Name { get; set; }
        public string Context { get; set; }
        public int SerialNumber { get; set; }
        public Guid FanfikFk { get; set; }
        public DateTime Created { get; set; }
        public string OptionIlistrutation { get; set; }

        public Fanfik Fanfik { get; set; }
        public ICollection<Rating> Ratings { get; set; }
    }
}
