﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models
{
    public partial class Comment
    {
        public Comment()
        {
            Likes = new HashSet<Like>();
        }

        public Guid Id { get; set; }
        public string Content { get; set; }
        public DateTime Created { get; set; }
        public string CreatorFk { get; set; }
        public Guid FanfikFk { get; set; }

        public ApplicationUser Creator { get; set; }
        public Fanfik Fanfik { get; set; }

        public ICollection<Like> Likes { get; set; }
    }
}
