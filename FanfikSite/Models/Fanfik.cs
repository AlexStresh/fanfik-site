﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanfikSite.Models
{
    public partial class Fanfik
    {
        public Fanfik()
        {
            Comments = new HashSet<Comment>();
            FanfikTags = new HashSet<FanfikTag>();
            Chapters = new HashSet<Chapter>();
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Shortcut { get; set; }
        public string Image { get; set; }
        public Guid GenreFk { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated {get; set;}
        public string CreatorFk { get; set; }

        public float Rating { get; set; }

        public ApplicationUser Creator { get; set; }
        public Genre Genre { get; set; }

        public ICollection<Comment> Comments { get; set; }
        public ICollection<FanfikTag> FanfikTags { get; set; }
        public ICollection<Chapter> Chapters { get; set; }
    }
}
