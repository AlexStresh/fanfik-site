﻿var page = 1;

function getPosts(pageNum, isNew, append = false) {
    console.log("Value " + isNew)
    $.ajax({
        url: "/Home/GetPosts",
        data: { page: pageNum, isNew: isNew },
        success: function (data) { loadData(data, isNew, append); }
    });
}

function loadData(data, param, append) {
    console.log(data)
    var container = $(param ? 'div.new' : 'div.popular');
    if (!append) {
        container.html('');
    }
    if (data != -1) {
        $.each(data, function (i, item) {
            var image = item.image == null ? '/images/no-image.jpg' : item.image
            var markup =
                `
                <div class='col-md-3 post'>
                    <div class="post_image" style="height: 215px; ">
                        <a href="/Post/Get/${item.id}"><img src="${image}" /></a>
                    </div>
                    <h4>${item.title}</h4>
                    <h5>${item.created}</h5>
                    <h4>Rating: ${item.rating}</h4>
               </div >
            `;
            container.append(markup);
        });
    }
}

function onClick() {
    if ($(this).hasClass('new')) {
        page = 1;
        $('div.popular').removeClass('popular').addClass('new');
    } else {
        page = 1;
        $('div.new').removeClass('new').addClass('popular');
    }

    $($(this).hasClass('new') ? 'button.popular' : 'button.new')
    $($(this).hasClass('new') ? 'button.popular' : 'button.new').prop('disabled', false);
    $(this).prop('disabled', true);
    console.log($(this));
    getPosts(0, $(this).hasClass('new'), false);
}

$(document).ready(function () {
    $('button.new').prop('disabled', true);

    $('button.new').on('click', onClick);
    $('button.popular').on('click', onClick);

    $(window).on("scroll", function () {
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
            var container = $('div.new') || $('div.popular');
            getPosts(page, container.hasClass('new'), true);
            page++;
        }
    });
});