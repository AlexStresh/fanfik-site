﻿$(document).ready(function () {
    var panels = $('.user-infos');
    var panelsButton = $('.dropdown-user');
    var deleteButton = $('.delete');
    var addAdminButton = $('.add-admin');
    var blockButton = $('.block');
    var dataFor;

    panels.hide();

    function deleteUser(id) {
        $.ajax({
            url: "/User/DeleteUser",
            type: "POST",
            data: { id: id },
            success: function () { return removeUserTemplate(id) }
        });
    }

    function addAdmin(id) {
        $.ajax({
            url: "/User/AddAdmin",
            type: "POST",
            data: { id: id },
            success: alert("Add Admin success")
        });
    }

    function blockUser(id) {
        $.ajax({
            url: "/User/BlockUser",
            type: "POST",
            data: { id: id },
            success: alert("Add block success")
        });
    }

    function unblockUser(id) {
        $.ajax({
            url: "/User/UnblockUser",
            type: "POST",
            data: { id: id },
            success: alert("Unblock success")
        });
    }


    function removeUserTemplate(id) {
        $('.user_' + id).remove();
        $('.info_' + id).remove();
    }

    panelsButton.click(function () {

        dataFor = $(this).attr('data-for');
        var idFor = $(dataFor);

        //current button
        var currentButton = $(this);
        idFor.slideToggle(400, function () {

            if (idFor.is(':visible')) {
                currentButton.html('<i class="icon-chevron-up text-muted"></i>');
            }
            else {
                currentButton.html('<i class="icon-chevron-down text-muted"></i>');
            }
        })
    });


    $('[data-toggle="tooltip"]').tooltip();

    $('.block').click(function (e) {
        var id = $(this).data(id).id;
        blockUser(id);
        $(this).css('display', 'none');
        $('.unblock').css('display', '');
    });

    $('.unblock').click(function (e) {
        var id = $(this).data(id).id;
        unblockUser(id);
        $(this).css('display', 'none');
        $('.block').css('display', '');
    });

    $('.delete').click(function (e) {
        var id = $(this).data(id).id;
        deleteUser(id);
    });

    $('.add-admin').click(function (e) {
        var id = $(this).data(id).id;
        console.log(id);
        addAdmin(id);
        $(this).toggleClass('invisible');
    });

});