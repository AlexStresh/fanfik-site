﻿$('.post_setting').click(function () {
    $('.invis').toggleClass('visible');
});

$('#toggle-box-checkbox').on('change', function () {
    if (this.checked) {
        $('.none').toggleClass('invisible');
        $('body').toggleClass('black');
        $('#progress').css('display', '')
    } else {
        $('.none').toggleClass('invisible');
        $('body').toggleClass('black');
        $('#progress').css('display', 'none')
    }
});


$('.arial').click(function () {
    $('pre').removeClass('georgia');
    $('pre').removeClass('roboto');
    $('pre').addClass('arial');
});

$('.georgia').click(function () {
    $('pre').removeClass('arial');
    $('pre').removeClass('roboto');
    $('pre').addClass('georgia');
});

$('.roboto').click(function () {
    $('pre').removeClass('georgia');
    $('pre').removeClass('arial');
    $('pre').addClass('roboto');
});



$('.14').click(function () {
    $('pre').removeClass('font_size_16');
    $('pre').removeClass('font_size_18');
    $('pre').addClass('font_size_14');
});
$('.16').click(function () {
    $('pre').removeClass('font_size_14');
    $('pre').removeClass('font_size_18');
    $('pre').addClass('font_size_16');
});
$('.18').click(function () {
    $('pre').removeClass('font_size_14');
    $('pre').removeClass('font_size_16');
    $('pre').addClass('font_size_18');
});


$(document).ready(function () {
    $(window).on("scroll", function () {
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        var progress = Math.round(scrollPosition / scrollHeight * 100);
        var div = document.getElementById('progress');
        div.innerHTML = progress + "";
    });
});