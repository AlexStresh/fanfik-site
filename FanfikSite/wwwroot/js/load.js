﻿function getNewComments(count, fanfikId) {
    $.ajax({
        url: "/Post/GetComments",
        data: { count: count, fanfikId: fanfikId },
        success: function (data) { loadData(data, count); }
    });
}

function loadData(data, count) {
    var container = $('div.add');
    console.log(data);
    if (data != -1) {
        $.each(data, function (i, item) {
            var markup =
                `
            <div class="row comment">
                <div class="col-md-1 user_photo">
                    <img src="/images/no_avatar.png" />
                </div>
                <div class="col-md-11">
                    <div class="text_comment">
                        <div class="comment_info">
                            <h5>${item.creator.email}</h5>
                            <h5>${item.created}</h5>
                        </div>
                        <p class="text-left comm">${item.content}</p>
                        <button class="btn btn-xs btn-info add-like" value="" data-id="${item.id}" style="width:35px"><span class="glyphicon glyphicon-heart">0</span></button>
                    </div>
                </div>
            </div>
        `;
            container.append(markup);
        });
        var newCount = Number(count) + Number(data.length);
        document.getElementById('comments-count').value = newCount;
    }
}

$(document).ready(() => {
    var fanfikId = document.getElementById("fanfikId").value;
    setInterval(function () {
        var count = document.getElementById('comments-count').value;
        console.log(count);
        getNewComments(count, fanfikId);

    },5000);
});