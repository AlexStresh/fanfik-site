﻿$(document).ready(() => {
    
    function deleteUser(id) {
        $.ajax({
            url: "/User/DeleteUser",
            type: "POST",
            data: { id: id },
            success: function() {return removeUserTemplate(id)}
        });
    }

    function blockUser(id, date) {
        $.ajax({
            url: "/User/BlockUser",
            type: "POST",
            data: { id: id, blockTime: date },
            success: alert("Add block success")
        });
    }

    function unblockUser(id) {
        $.ajax({
            url: "/User/UnblockUser",
            type: "POST",
            data: { id: id },
            success: alert("Unblock success")
        });
    }

    function addAdmin(id) {
        $.ajax({
            url: "/User/AddAdmin",
            type: "POST",
            data: { id: id },
            success: alert("Add Admin success")
        });
    }

    function removeUserTemplate(id) {
        $('.user_' + id).remove();
    }

    $('.delete').on('click', function () {
        var id = $(this).data('id');
        deleteUser(id);
        removeUserTemplate(id);
    });

    $('.block').on('click', function () {
        var id = $(this).data('id');
        var date = document.getElementsByName('date_'+id)[0].value;
        console.log(date);
        console.log()
        blockUser(id, date);
        $('.date_' + id).remove();
        $(this).replaceWith(`<a class="unblock" data-id="${id}">Unblock</a>`);
    });

    $('.unblock').on('click', function () {
        var id = $(this).data('id');
        unblockUser(id);
        $(this).replaceWith(`<a class="block" data-id="${id}">Block</a><input type="date" class="date_${id}" name="date_${id}">`);
    });

    $('.add-admin').on('click', function () {
        var id = $(this).data('id');
        addAdmin(id);
        removeUserTemplate(id);
    });
});