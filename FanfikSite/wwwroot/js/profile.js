﻿function chageTheme() {
    var userId = document.getElementById("userId").value;
    console.log(userId);
    $.ajax({
        url: "/User/ChangeTheme",
        data: { id: userId },
        success: function (data) { reload(); }
    });
}

function reload() {
    setTimeout(function () {
        location.reload();
    }, 2000);
}