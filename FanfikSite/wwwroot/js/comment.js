﻿function sendComments(fanfikId, comment) {
    $.ajax({
        url: "/Post/AddComment",
        type: "POST",
        data: { fanfikId: fanfikId, context: comment },
        success: function (data) { loadData(data); }
    });
}

function addLike(commentId, button) {
    $.ajax({
        url: "/Post/AddLike",
        type: "POST",
        data: { commentId: commentId },
        success: function (data) { loadLike(data, button); }
    });
}

function loadLike(data, button) {
    console.log(button);
    if (data != -1) {
        button[0].innerHTML = `<span class="glyphicon glyphicon-heart">${data}</span>`;
        button[0].disabled = true;
    }
}

function loadData(data) {
    //console.log(data);
    var container = $('div.add');
    /*if (data != -1) {
        var markup =
            `
            <div class="row comment">
                <div class="col-md-1 user_photo">
                    <img src="/images/no_avatar.png" />
                </div>
                <div class="col-md-11">
                    <div class="text_comment">
                        <div class="comment_info">
                            <h5>${data.creator.userName}</h5>
                            <h5>${data.created}</h5>
                        </div>
                        <p class="text-left comm">${data.content}</p>
                        <button class="btn btn-xs btn-info add-like" value="" data-id="${data.id}" style="width:35px"><span class="glyphicon glyphicon-heart">0</span></button>
                    </div>
                </div>
            </div>
        `;
        container.append(markup);
    }*/
}

function onClick() {
    content = document.getElementById("commentId").value;
    fanfikId = document.getElementById("fanfikId").value;
    sendComments(fanfikId, content);
    document.getElementById("commentId").value = "";
};

$(document).ready(function () {

    $('button.submit').on('click', onClick);

    $('.add-like').click(function (e) {
        var id = $(this).data(id).id;
        addLike(id, $(this));
    });
});