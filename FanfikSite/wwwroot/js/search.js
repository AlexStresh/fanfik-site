﻿$(document).ready(function () {
    function search(e) {
        console.log(e.target.value);
        $.ajax({
            url: "/Home/SearchAjax",
            type: "POST",
            data: { search: e.target.value },
            success: function(data) { return loadResults(data) }
        });
    }

    function loadResults(data) {
        console.log(data);
        var html = '<ul class="search-data">';
         
        $.each(data, function (i, item) {
            html +=
                `
                 <li class="li"><a href="/Post/Get/${item.id}"><span>${item.title}</span></a></li>
            `;
        });
        html += '</ul>';

        $('.search-results').html(html);
    }

    $('#search').on('input', search);
});