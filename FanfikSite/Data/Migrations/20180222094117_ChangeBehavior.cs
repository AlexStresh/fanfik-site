﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FanfikSite.Data.Migrations
{
    public partial class ChangeBehavior : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Chapter_Fanfik_FanfikFk",
                table: "Chapter");

            migrationBuilder.AddForeignKey(
                name: "FK_Chapter_Fanfik_FanfikFk",
                table: "Chapter",
                column: "FanfikFk",
                principalTable: "Fanfik",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Chapter_Fanfik_FanfikFk",
                table: "Chapter");

            migrationBuilder.AddForeignKey(
                name: "FK_Chapter_Fanfik_FanfikFk",
                table: "Chapter",
                column: "FanfikFk",
                principalTable: "Fanfik",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
