﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using FanfikSite.Models;

namespace FanfikSite.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        public virtual DbSet<Chapter> Chapter { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<Fanfik> Fanfik { get; set; }
        public virtual DbSet<FanfikTag> FanfikTag { get; set; }
        public virtual DbSet<Rating> Rating { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<Genre> Genre { get; set; }
        public virtual DbSet<Like> Like { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Cascade;
            }

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Chapter>(entity => 
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Name).IsRequired();

                entity.HasOne(d => d.Fanfik)
                    .WithMany(p => p.Chapters)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasForeignKey(d => d.FanfikFk);

            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Content).IsRequired();

                entity.Property(e => e.Created)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.HasOne(d => d.Creator)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.CreatorFk)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Fanfik)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.FanfikFk)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Like>(entity =>
            {
                entity.Property(l => l.Id).ValueGeneratedNever();

                entity.HasOne(l => l.Creator)
                    .WithMany(p => p.Likes)
                    .HasForeignKey(l => l.CreatorFk)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(l => l.Comment)
                    .WithMany(p => p.Likes)
                    .HasForeignKey(l => l.CommnetFk)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Fanfik>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Updated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Title).IsRequired();

                entity.HasOne(d => d.Creator)
                    .WithMany(p => p.Fanfiks)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasForeignKey(d => d.CreatorFk);

                entity.HasOne(d => d.Genre)
                     .WithMany(p => p.Fanfiks)
                     .OnDelete(DeleteBehavior.ClientSetNull)
                     .HasForeignKey(d => d.GenreFk);

            });

            modelBuilder.Entity<FanfikTag>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.Fanfik)
                    .WithMany(p => p.FanfikTags)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasForeignKey(d => d.FanfikFk);

                entity.HasOne(d => d.Tag)
                     .WithMany(p => p.FanfikTags)
                     .OnDelete(DeleteBehavior.ClientSetNull)
                     .HasForeignKey(d => d.TagFk);

            });

            modelBuilder.Entity<Genre>(entity => 
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(120);
            });

            modelBuilder.Entity<Rating>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Chapter)
                    .WithMany(p => p.Ratings)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasForeignKey(d => d.ChapterFk);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Ratings)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasForeignKey(d => d.UserFk);
            });

            modelBuilder.Entity<Tag>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).IsRequired();
            });

        }
    }
}
