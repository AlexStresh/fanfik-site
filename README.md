# FANFICSITE #

**FanficSite** is a website that gives the opportunity to share or publish some story based on popular 
original literary works, works of motion picture art (movies, television series, anime, etc.), 
comic books (including manga), and computer games, etc.

Developed on ASP.NET Core 2.0

## Instalation guide ##

Instalation not required(website deploy on hosting "")

If you want view code download or clone this repository:

``` bash 
git clone https://Sawanowi4_Alexey@bitbucket.org/aldestr/fanfiksite.git
``` 

## Usage guide: ##

To access this site follow this link <link>

If you want sign in click "sing in" top right and enter login and password or sign in with help socialnetworks

If you want to create account, click register, enter username email, password and confirm password.
After that you should click link in letter in entered email

If you want search some story enter the name story in the search box, for example:
> Harry Potter and the Bloody Orgy in Martian Hell

If you want change language click on icon flag top right and choose langusge

If you want change theme click on icon theme top right and choose theme

If you want rate story click the required number of stars nearly to the chapter title(1 star = 1 point)

If you want download full story in PDF click on "Save PDF" nearly title story

More information you can see click Help on main page of website

## Modules discription ##
